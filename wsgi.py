from run import app
from werkzeug.contrib.fixers import ProxyFix

if __name__ == "__main__":
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run(host='0.0.0.0', port=5000, threaded=True, use_reloader=False)