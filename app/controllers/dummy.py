'''  '''
import os
from flask import Blueprint, request, abort, jsonify

dummy = Blueprint('dummy_blueprint', __name__, url_prefix='/dummy')

@dummy.route("", methods=["GET"])
def get_dummies():
    ''' Get list of dummies '''
    return jsonify([])

@dummy.route("/<id>", methods=["GET"])
def get_dummy(id):
    ''' Get dummy '''
    return jsonify({ 'id': id })
