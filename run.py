''' API main and routes'''
import os
from flask import Flask, jsonify
from flask_cors import CORS
from flask_jwt_extended import JWTManager
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk import capture_exception
from app.controllers.dummy import dummy as dummy_controller
from app.const import SECRET_KEY

# Flask app creation
app = Flask(__name__)
cors = CORS(app, expose_headers=['x-count-pages', 'x-count-items'])
jwt = JWTManager(app)

# Config - JWT
app.config['JWT_SECRET_KEY'] = SECRET_KEY

# Config - Sentry
if app.config.get("SENTRY_DSN", None) is not None:
    sentry_sdk.init(
        dsn=app.config.get("SENTRY_DSN"),
        integrations=[FlaskIntegration()]
    )
else:
    print('no sentry')

# Error handling - Bad request (400)
@app.errorhandler(400)
def bad_request(error):
    print("Error 400 - ", str(error))
    capture_exception(error)
    return jsonify(error=str(error)), 400

# Error handling - Not found (404)
@app.errorhandler(404)
def not_found(error):
    print("Error 404 - ", str(error))
    capture_exception(error)
    return jsonify(error=str(error)), 404

# Error handling - Internal error (500)
@app.errorhandler(Exception)
def handle_error(error):
    print("Error 500 - ", str(error))
    capture_exception(error)
    return jsonify(error=str(error)), 500

# Routes definition, DB initialization
def initialize_app(flask_app):
    flask_app.register_blueprint(dummy_controller)

initialize_app(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, threaded=True, use_reloader=False)
