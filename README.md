# Base Project - Python Flask

Basic backend project using Python and Flask.

## Versions

- For a version having file bucket, checkout to branch _*base_bucket*_ 

- For a version having database integration with _sqlalchemy_, checkout to branch _*base_sqlalchemy*_

## Setup

1. Create a virtual environment using _virtualenv_

    ```virtualenv env```

2. Activate it

    ```source env/bin/activate```

3. Install requirements

    ```pip install -r requirements.txt```

## Run

After setting up, run:

```python run.py```

